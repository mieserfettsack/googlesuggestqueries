# googlesuggestqueries

## Description

A small tool to get all available suggestions to a given word and export them to a csv file. Using suggestqueries.google.com to get suggestions. The entire app is based on docker and ddev.

## Usage

docker and ddev is required to use this tool. Go to the project folder and execute the following command:

`ddev start`

After ddev started  successfully open the following url in your favorite browser:

`https://googlesuggestqueries.ddev.site`

Type a word in the input field and press **Vorschläge abholen (a-z)**. The app is not asynchronous so please be patient, because nothing will happen on the screen during run.

### Nummern hinzufügen

Use **Nummern hinzufügen** to add number to the suggestions. If you searched for **I like** with **Nummern hinzufügen** activated you will recieve results like: **I like 1337**, **I like 10000000**

### Vorschläge abholen (aa-zz)

Use **Vorschläge abholen (aa-zz)** to retrieve suggestions for 2 letters. Please note, that this options can take very long. Up to 7 minutes.

### Status Ausgabe aktivieren

Use **Status Ausgabe aktivieren** to output debug. This output is not any more. It will appear after suggestions was tested.

### Sucheinstellungen zurücksetzen

**Sucheinstellungen zurücksetzen** will reset all search settings.

## Screenshots

### Using googlesuggestqueries in browser

![Using googlesuggestqueries to retrive suggestion to a given word](./Resources/Public/Images/use_googlesuggestqueries_in_browser.png?raw=true "Using googlesuggestqueries to retrive suggestion to a given word")

### Exported result of googlesuggestqueries as text

The last export of google suggestions is located here: `./public/lastSuggestions.txt`

```
suchwort markieren
suchwort mit den meisten treffern
suchwort ne demek
suchwort targeting
suchwort trends
google suchwort tool
google suchwort vergleich
bergisches wochenende suchwort
bergisches wochenende stichwort der wochem
```

### Exported result of googlesuggestqueries as csv opened in LibreOffice Calc

![Exported result of googlesuggestqueries as csv opened in LibreOffice Calc](./Resources/Public/Images/export_result_of_googlesuggestqueries_as_csv.png?raw=true "Exported result of googlesuggestqueries as csv opened in LibreOffice Calc")