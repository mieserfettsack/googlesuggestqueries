<?php

require __DIR__ . '/../vendor/autoload.php';

header('Content-Encoding: UTF-8');
header('Content-type: text/csv, charset=utf-8');
header('Content-Disposition: attachment; filename="' . URLify::filter(isset($_GET['searchWord']) && !empty($_GET['searchWord']) ? htmlspecialchars($_GET['searchWord']) : "keinwort", 60, 'de') . '-' . date('Y-m-d') . '.csv"');
header('Content-Transfer-Encoding: binary');
header('Pragma: no-cache');
header('Expires: 0');
echo "\xEF\xBB\xBF"; // UTF-8 BOM
readfile('../public/lastSuggestions.txt');
