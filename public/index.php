<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>google suggestion tool</title>
    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/CSS/styles.css">
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</head>
<body>

<div class="bs-example" data-example-id="basic-forms">
    <form>
        <div class="form-group">
            <label for="searchWord">Suchwort</label>
            <input class="form-control" id="searchWord" name="searchWord" placeholder="Suchwort"
                   value="<?php if (isset($_GET['searchWord']) && !empty($_GET['searchWord'])) {
                       echo $_GET['searchWord'];
                   } ?>">
        </div>

        <div class="checkbox">
            <label><input type="checkbox" name="statusOutput"
                          value="1" <?php if (isset($_GET['statusOutput']) && !empty($_GET['statusOutput'])) {
                    echo 'checked';
                } ?>>Status Ausgabe aktivieren</label>
        </div>

        <div class="checkbox">
            <label><input type="checkbox" name="addNumber"
                          value="1" <?php if (isset($_GET['addNumber']) && !empty($_GET['addNumber'])) {
                    echo 'checked';
                } ?>>Nummern hinzufügen</label>
        </div>

        <button type="submit" class="btn btn-default" value="az" name="action">Vorschläge abholen (a-z)</button>
        <button type="submit" class="btn btn-default" value="aazz" name="action">Vorschläge abholen (aa-zz)</button>
        <a href="<?php $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2); echo '//' . $_SERVER['HTTP_HOST'] . $uri_parts[0]; ?>" class="btn btn-default">Sucheinstellungen zurücksetzen</a>
    </form>
</div>

<?php
if (isset($_GET['searchWord']) && isset($_GET['action']) && !empty($_GET['searchWord']) && !empty($_GET['action'])) {
    include("../application/createCSVFromGoogleSuggestion.php");
}
?>
</body>
</html>
