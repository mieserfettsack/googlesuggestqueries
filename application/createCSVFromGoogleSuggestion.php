<?php
// make sure that max_execution_time is at least 360 seconds high
// an aa-zz run with numbers takes around 6 minutes
ini_set('max_execution_time', 2400);
//header('Content-Encoding: none;');
header('X-Accel-Buffering: no');
define('LASTSUGGESTIONSFILE', './lastSuggestions.txt');
$errorCount = false;
$limitCount = false;

$preparedSuggestions = [];
$allSuggestions = [];
$searchWord = (isset($_GET['searchWord']) && !empty($_GET['searchWord']) ? htmlspecialchars($_GET['searchWord']) : false);
$statusOutput = (isset($_GET['statusOutput']) && !empty($_GET['statusOutput']) ? htmlspecialchars($_GET['statusOutput']) : false);
$action = (isset($_GET['action']) && !empty($_GET['action']) ? htmlspecialchars($_GET['action']) : false);

/**
 * builds a proper array out of $stringsToAppend
 * it deals with number and aa-zz switches as well
 *
 * @param $searchWord
 * @return array|bool
 */
function prepareSuggestionsArray($searchWord)
{
    $action = (isset($_GET['action']) && !empty($_GET['action']) ? htmlspecialchars($_GET['action']) : false);
    $addNumber = (isset($_GET['addNumber']) && !empty($_GET['addNumber']) ? htmlspecialchars($_GET['addNumber']) : false);

    if ($searchWord) {

        $preparedKeywords = [];
        $stringsToAppend = [
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ä', 'ö', 'ü'
        ];

        $numberToAppend = [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9
        ];

        if ($addNumber) {
            $stringsToAppend = array_merge($stringsToAppend, $numberToAppend);
        }

        foreach ($stringsToAppend as $singleAppend) {
            if ($action == 'az') {
                $preparedKeywords[] = $searchWord . " " . $singleAppend;
            }

            if ($action == 'aazz') {
                foreach ($stringsToAppend as $singleAppendLevel2) {
                    $preparedKeywords[] = $searchWord . " " . $singleAppend . $singleAppendLevel2;
                }
            }
        }

        array_unshift($preparedKeywords, $searchWord);

        return $preparedKeywords;
    }
    return false;
}

/**
 * helper to get the http status as integer for a given url
 *
 * @param $url
 * @return string
 */
function get_http_response_code($url)
{
    $headers = get_headers($url);
    return substr($headers[0], 9, 3);
}


/**
 * send a request to google to retrieve suggestion for a given searchword
 * on success it returns an array with maximum of 10 suggestions
 *
 * @param $searchWord
 * @return array|bool|string
 */
function getKeywordSuggestionsFromGoogle($searchWord)
{
    $currentResponseCode = get_http_response_code('https://suggestqueries.google.com/complete/search?client=firefox&hl=de&q=' . urlencode($searchWord));

    switch ($currentResponseCode) {
        case 200:
            $data = file_get_contents('https://suggestqueries.google.com/complete/search?client=firefox&hl=de&q=' . urlencode($searchWord));

            if (
                // we have correct suggetions delivered by google
                // if $data is a valid json
                // and [1] is not empty
                ($data = json_decode($data, true)) !== null &&
                !empty(array_filter($data[1]))
            ) {
                return $data[1];
            } else {
                return "empty";
            }
            break;
        case 403:
            return "limit";
            break;
        default:
            return "error";
    }
}


// main actions starts here

// empty LASTSUGGESTIONSFILE file each time
file_put_contents(LASTSUGGESTIONSFILE, '');


$preparedSuggestions = prepareSuggestionsArray($searchWord);


if (!$statusOutput) {
    echo '<div id="loadingStatusWrap"><img id="loadingStatusImage" src="/Images/gears.gif"></div>';
}

if ($statusOutput) {
    echo '<div class="result">';
}

foreach ($preparedSuggestions as $singleSuggestions) {
    unset($tempArray);
    $tempArray = getKeywordSuggestionsFromGoogle($singleSuggestions);

    if (is_array($tempArray)) {
        foreach ($tempArray as $singleSuggestionFromGoogle) {
            $allSuggestions[] = utf8_encode($singleSuggestionFromGoogle);
        }

        if ($statusOutput) {
            echo '<p class="eachResult bg-success">Treffer für: ' . $singleSuggestions . '</p>';
        }
    } else {
        if ($tempArray == 'empty') {
            if ($statusOutput) {
                echo '<p class="eachResult bg-info">Keine Treffer für: ' . $singleSuggestions . '</p>';
            }
        }
        elseif ($tempArray == 'limit') {
            if ($statusOutput) {
                echo '<p class="eachResult bg-danger">Tägliches Limit erreicht!</p>';
            }
            $limitCount = true;
        }
        else {
            if ($statusOutput) {
                echo '<p class="eachResult bg-danger">Fehler bei: ' . $singleSuggestions . '</p>';
            }
            $errorCount = true;
        }
    }

    usleep(150000);
    flush();
    ob_flush();
}

if (!$statusOutput) {
    echo '<style>#loadingStatusWrap{display: none}</style>';
}

if ($statusOutput) {
    echo "</div>";
}

// remove the search word from array itself
$index = array_search($searchWord, $allSuggestions);
if ($index !== FALSE) {
    unset($allSuggestions[$index]);
}

// remove all duplicates
$allSuggestions = array_unique($allSuggestions);

// write all suggestions to LASTSUGGESTIONSFILE
file_put_contents(LASTSUGGESTIONSFILE, implode(PHP_EOL, $allSuggestions));

if ($errorCount) {
    echo '<p class="errorText bg-danger">Es gab einen Fehler beim Abfragen der Vorschläge. Google hat technische Schwierigkeiten.</p>';
} elseif ($limitCount) {
    echo '<p class="errorText bg-danger">Das tägliche Limit wurde erreicht. Keine Abfragen mehr möglich!</p>';
} else {
    echo '<p class="errorText bg-success">Der Durchlauf für <b>' . $searchWord . '</b> war erfolgreich. Die CSV-Datei kann nun heruntergeladen werden!</p>';
}

echo '<a class="btn btn-info" target="_blank" href="/downloadCSV.php?searchWord=' . $searchWord . '">CSV Liste herunterladen</a>';

